import React, { Component } from 'react';


export default class LoaderComponent extends Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	render() {
		return (
			<div className='loader-wrapper'>
				<div className='loader-container'>
					<div className='loader'></div>
				</div>
			</div>
		);
	}


}