try {
  require('../sass/main.scss');
} catch(error) {
};

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import 'whatwg-fetch';
require('babel-runtime/core-js/promise').default = require('es6-promise-promise');

import AppController from './controllers/AppController';

class Main extends React.Component {

	render() {
		return (
			<AppController />
		);
	}

}

ReactDOM.render(<Main/>, document.getElementById('root'));