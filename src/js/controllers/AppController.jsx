import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';

import LoaderComponent from '../components/LoaderComponent';
import EntryController from './EntryController';


export default class AppController extends Component {

	constructor(props) {
		super(props);
		this.state = {
			loading: true
		};
	}

	componentWillMount() {
		this.fetchOptions();
	}

	render() {
		return (
			<section className='main-content'>
				{this.state.loading ? this._renderLoader() : this._renderContent()}
			</section>
		);
	}

	_renderContent() {
		return (
			<Router>
				<Switch>
					<Route exact path='/' render={() => <EntryController verbs={this.state.data} shuffleArray={this.shuffleArray} />} />
				</Switch>
			</Router>
		);
	}

	_renderLoader() {
		return (
			<LoaderComponent />
		);
	}


	fetchOptions = () => {
		return fetch('./json/options.json')
        .then(response => {
        	return response.json()
        })
        .then(data => {
        	console.log('Success Fetching Options: ', data)
        	this.setState({
        		data: this.shuffleArray(data.verbs)
        	}, () => {
        		setTimeout(() => {
        			this.setState({
	               		loading: false
	        		});
        		}, 1500);
        	});
        })
        .catch(err => {
        	setTimeout(() => {
    			this.setState({
               		loading: false
        		});
    		}, 1500);
        	console.log('Error Fetching Options: ', err);
        })
	}

	shuffleArray = (array) => {
	    for (let i = array.length - 1; i > 0; i--) {
	        const j = Math.floor(Math.random() * (i + 1));
	        [array[i], array[j]] = [array[j], array[i]];
	    }
	    return array;
	}

}