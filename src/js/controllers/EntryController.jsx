import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { TweenLite } from 'gsap';

import ReactSVG from 'react-svg';


export default class EntryController extends Component {

	constructor(props) {
		super(props);
		this.state = {
			shuffledVerbs: props.verbs,
			currentVerb: ''
		};
	}

	componentDidMount() {
		this.updateDimensions();
		window.addEventListener('resize', this.updateDimensions);
	}

	componentWillUnmount() {
    	window.removeEventListener('resize', this.updateDimensions);
  	}

  	updateDimensions = () => {
  		this.setState({
  			windowSize: window.innerWidth
  		});
  	}

	render() {
		return (
			<section className='entry-wrapper bg'>
				<div className='entry-container'>
					<div className='entry-content'>
						<div className='title-wrapper'>
							<div className='title-container'>
								<ReactSVG
								    path={require('../../assets/images/main_logo.svg')}
								    wrapperClassName='title-image-wrapper'
								    className='title-image'>
								</ReactSVG>
								<div className='verb-wrapper'>
									<div className='verb-container'>
										<h3 className='verb'>
											{this.state.currentVerb}
										</h3>
									</div>
								</div>
							</div>
						</div>
						<div className='wheel-wrapper'>
							<div className='wheel-container'>
								<div className='wheel-image-wrapper'>
									<div className='wheel-image-container'>
										<ReactSVG
										 	ref={wheel => this.wheel = wheel}
										    path={require('../../assets/images/wheel.svg')}
										    className='wheel-image'>
										</ReactSVG>
										<button className='wheel_btn-image-wrapper' onClick={this._spinTheWheel}>
											<ReactSVG
											    path={require('../../assets/images/wheel_btn.svg')}
											    wrapperClassName='wheel_btn-image-container'
											    className='wheel_btn-image'>
											</ReactSVG>
										</button>
									</div>
									<ReactSVG
									    path={require('../../assets/images/wheel_shadow.svg')}
									    wrapperClassName='wheel_shadow-image-container'
									    className='wheel_shadow-image'>
									</ReactSVG>
								</div>
								<div className='spinning_msg-image-wrapper'>
									<ReactSVG
									    path={this.setSpinningMessage()}
									    wrapperClassName='spinning_msg-image-container'
									    className='spinning_msg-image'>
									</ReactSVG>
								</div>
								<div className='verb-wrapper'>
									<div className='verb-container'>
										<h3 className='verb'>
											{this.state.currentVerb}
										</h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}

	_spinTheWheel = (ev) => {
		var el = this.wheel.container.firstChild.firstChild;
		TweenLite.to(el, 3, {
			rotation: 720,
			transformOrigin: 'center',
			onComplete: this.onRotationComplete
		});
	}

	onRotationComplete = () => {
		var el = this.wheel.container.firstChild.firstChild;
		TweenLite.set(el, {
			rotation: 0,
			transformOrigin: 'center'
		});
		this.setNewVerbs();
	}

	setNewVerbs() {
		if(this.state.shuffledVerbs.length === 1) {
			this.setState({
				shuffledVerbs: this.props.shuffleArray(this.props.verbs)
			}, () => {
				this.setState({
					currentVerb: this.state.shuffledVerbs[0]
				});
			});
		} else {
			this.setState({
				shuffledVerbs: this.state.shuffledVerbs.slice(1)
			}, () => {
				this.setState({
					currentVerb: this.state.shuffledVerbs[0]
				});
			});
		}
	}

	setSpinningMessage() {
		if(this.state.windowSize > 576) {
			return require('../../assets/images/spinning_msg.svg');
		} else {
			return require('../../assets/images/spinning_msg_small.svg');
		}
	}


}