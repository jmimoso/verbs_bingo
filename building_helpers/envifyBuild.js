const { join } = require('path');
const assign   = require('object-assign');
const { logError, logInfo, colors } = require('./debugOutup');


/**
 * loadEnviFile - Resolves the environment configurations localization, 
 * loads the file, and return the content of it.
 * @param {String} basePath, absolute path for the project folder.
 * @returns {Object} environment configurations.
 */
const loadEnviFile = (basePath) => {
	let env       		= process.env;
	let envType   		= env.NODE_ENV || 'production';
	let environmentDir  = env.npm_package_config_environment_dir;
	
	env._basePath = basePath;

	try {
		logInfo('> WEBPACK:BUILD STARTS AS: ' + colors(envType, 'red'));
		return require(join(basePath, environmentDir, envType.trim()))(env, basePath);
	}catch(e) {
		logError(e);
		return function(){return{}};
	};
};

/**
 * mergeProps - Merges the properties of two objects,
 * and return a single unified object.
 * @param {Object} config, Object with properties to be overwritten or added
 * @param {Object} envConfig, set of properties to merger
 * @returns {Object} newConfig, new object with the merged properties
 */
const mergeProps = (config, envConfig) => {
	let newValue;
	let newConfig = assign({}, config);
	
	if(envConfig){
		for(var key in envConfig){

			if( typeof envConfig[key] === 'object' && !Array.isArray(envConfig[key])) {
				newValue = mergeProps(config[key], envConfig[key]);
			}else if( Array.isArray(envConfig[key]) ){
				newValue = [].concat(config[key], envConfig[key]);
			}else{
				newValue = envConfig[key]
			};

			newConfig[key] = newValue;
		};
	};

	return newConfig;
};


exports.loadEnviFile = loadEnviFile;
exports.mergeProps   = mergeProps;